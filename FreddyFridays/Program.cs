﻿using System;

namespace FreddyFridays
{
    class Program
    {
        static void Main(string[] args)
        {
            bool stop = false;

            do
            {
                Console.WriteLine("Enter a year between 2021-3000:");
                int year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine($"The number of occurences of Friday the 13th in the year {year}:");

                DateTime startDate = new DateTime(year, 01, 13);
                DateTime endDate = new DateTime(year, 12, 13);
                int freddyFridays = 0;

                while (startDate < endDate)
                {
                    if (startDate.Day == 13 && startDate.DayOfWeek == DayOfWeek.Friday)
                    {
                        freddyFridays++;
                    }
                    startDate = startDate.AddMonths(1);
                }
                Console.WriteLine(freddyFridays);
                Console.WriteLine("Do you want to stop?(y/n)");
                string s = Console.ReadLine();
                if(s == "y") { Console.WriteLine("Stopping..."); stop = false; }
            }
            while (stop == true);
        }
    }
}
